<div style="clear: both; height: 20px;">&nbsp;</div>
</div></div>
<!-- end page -->
</div>
<!-- If you'd like to support WordPress, having the "powered by" link somewhere -->
<!-- on your blog is the best way, it's our only promotion or advertising.      -->
<div id="footer">
	<div id="footer-wrap">
		<p id="legal">Designed by <a href="http://www.condorsolutions.ca/">Condor Solutions Real Estate Web Design</a></p>
	</div>
</div>

<?php wp_footer(); ?>
</body>
</html>
