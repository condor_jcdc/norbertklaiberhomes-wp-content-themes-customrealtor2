<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /><!-- leave this for stats -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>
</head>
<body>
<div id="wrapper">

<div id="header">
  <div id="top">
    <div id="logo1"><a href="http://www.ssremedicinehat.ca/" target="_blank"><img src="/wp-content/themes/customrealtor2/images/logo-signature-service.png" alt="Signature Service Real Estate" border="0" /></a></div>
    <div id="logo-realtor"><img src="/wp-content/themes/customrealtor2/images/logo-nk.png" alt="Norber Klaiber" /></div>
    <div id="logo2"><a href="http://www.brostdevelopments.com/" target="_blank"><img src="/wp-content/themes/customrealtor2/images/logo-brost.gif" alt="Brost Homes" border="0" /></a></div>
  </div>
	</div>
</div>
<!-- end header -->

	<div id="menu">
          <?php /* Widgetized sidebar */
          if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('my_mega_menu') ) : ?><?php endif; ?>
	</div>
	<!-- end menu -->

<div id="page"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Post Widget') ) : ?>	<?php endif; ?>
<!--<div id="header-big"><div>	</div>
       	<div id="featured-property">
    	<div id="property-thumb">&nbsp;&nbsp;<a href="http://www.norbertklaiberhomes.com/brost-homes/3-126-ranchman-cr-ne"><img src="/wp-content/themes/customrealtor2/images/property-thumb.jpg" width="144" height="108" border="0" /></a></div>
    	<span class="propertyText"> <strong>3-126 RANCHMAN CR NE</strong><br />
MLS&reg; 14288 $189,900<br />
<br />
<a href="http://www.norbertklaiberhomes.com/contact">Contact Norbert for Details</a></span>
    </div>
    <div id="open-house1">
    <div id="property-thumb">&nbsp;&nbsp;<a href="http://www.norbertklaiberhomes.com/brost-homes/1161-queen-st-se"><img src="/wp-content/themes/customrealtor2/images/property-thumb2.jpg" width="144" height="108" border="0" /></a></div>
    <span class="propertyText"> <strong>1161 QUEEN ST SE</strong><br />
MLS&reg; 13685  CAD69,900 <br />
<br />
<a href="http://www.norbertklaiberhomes.com/contact">Contact Norbert for Details</a></span>
    </div>
    <div id="open-house2">
    <div id="property-thumb">&nbsp;&nbsp;<a href="http://www.norbertklaiberhomes.com/brost-homes/3-126-ranchman-cr-ne"><img src="/wp-content/themes/customrealtor2/images/property-thumb.jpg" width="144" height="108" border="0" /></a></div>
<span class="propertyText">
    <strong>2-126 RANCHMAN CR NE</strong><br />
      MLS&reg; 15005 CAD189,900<br />
      <br />
      <a href="http://www.norbertklaiberhomes.com/contact">Contact Norbert for Details</a> </span>
    </div>
</div>-->
<div style="clear:both;">&nbsp;</div>
<div id="page-bgtop">